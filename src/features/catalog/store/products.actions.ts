import { del, get, patch, post } from '../services/http.service';

import { AppThunk } from '../../../App';
import { Product } from '../model/product';
import { addProductSuccess, getProductsSuccess, toggleProductVisibility, deleteProductSuccess } from './products.store';


export const getProductsHttp = (): AppThunk => async dispatch => {
  try {
   const response = await get('http://localhost:3001/products');
   dispatch(getProductsSuccess(response))
  } catch (err) {
    // dispatch error
  }
};

export const addProductHttp = (
  product: Omit<Product, 'id' | 'visibility'>
): AppThunk => async dispatch => {

  const payload: Omit<Product, 'id'> = {
    ...product,
    visibility: false
  };

  try {
    const newProduct = await post('http://localhost:3001/products', payload);
    dispatch(addProductSuccess(newProduct))
  } catch (err) {
    // dispatch error
  }
};

export const deleteProductHttp = (
  id: number
): AppThunk => async dispatch => {
  try {
    await del(`http://localhost:3001/products/${id}`);
    dispatch(deleteProductSuccess(id))
  } catch (err) {
    // dispatch error
  }
};

export const toggleProductHttp = (
  product: Product
): AppThunk => async dispatch => {
  try {
    const updatedProduct: Product = {
      ...product,
      visibility: !product.visibility
    };
    const response = await patch(`http://localhost:3001/products/${product.id}`, updatedProduct);
    dispatch(toggleProductVisibility(response))
  } catch (err) {
    // dispatch error
  }
};



